#pragma once
#include <iostream>
#include "LineSegment.h"
#include <vector>
#include "Curve.h"

class RobotPath
{
public:
	RobotPath();
	void AddCurve(Curve curve);
	void AddStraight(double distance);
	void ShowPath();
	~RobotPath();
private:
	std::vector<std::vector<double>> Path;
};

