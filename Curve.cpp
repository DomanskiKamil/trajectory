#include "Curve.h"


Curve::Curve() {};

Curve::Curve(LineSegment FirstLine, LineSegment SecondLine)
{
	Point crossPoint;
	this->firstStraight = false;
	if (FirstLine.GetLengthME() >= SecondLine.GetLengthBM()) {
		this->firstStraight = true;
	}
	LineSegment perpendicularToFirstLine = FirstLine.Perpendicular(this->CurveStartPoint(FirstLine, SecondLine));
	LineSegment perpendicularToSecondLine = SecondLine.Perpendicular(this->CurveEndPoint(FirstLine, SecondLine));

	this->center.y = (perpendicularToFirstLine.GetA() * perpendicularToSecondLine.GetC() - perpendicularToFirstLine.GetC() * perpendicularToSecondLine.GetA()) /
		(perpendicularToFirstLine.GetB() * perpendicularToSecondLine.GetA() - perpendicularToFirstLine.GetA() * perpendicularToSecondLine.GetB());
	
	this->center.x = (-perpendicularToSecondLine.GetB() * this->center.y - perpendicularToSecondLine.GetC()) / perpendicularToSecondLine.GetA();
	
	this->radius = sqrt(pow(this->center.x - FirstLine.GetMiddlePoint().x, 2) + pow(this->center.y - FirstLine.GetMiddlePoint().y, 2));
	
	this->angle = 2 * acos(this->radius / sqrt(pow(this->center.x - FirstLine.GetEnd().x, 2) + pow(this->center.y - FirstLine.GetEnd().y, 2)));

	if (FirstLine.GetA()*this->center.x + FirstLine.GetB()*this->center.y + FirstLine.GetC() > 0) this->curveOnTheRightSide = false;
	else this->curveOnTheRightSide=true;

	this->SetLengthOfStraight(FirstLine, SecondLine);
}

void Curve::SetLengthOfStraight(LineSegment FirstLine, LineSegment SecondLine) {
	if (firstStraight) {
		this->lenghtOfStraight = FirstLine.GetLengthME() - SecondLine.GetLengthBM();
	}
	else {
		this->lenghtOfStraight = SecondLine.GetLengthBM() - FirstLine.GetLengthME();
	}
}

Point Curve::CurveStartPoint(LineSegment FirstLine, LineSegment SecondLine) {
	if (this->firstStraight) {
		return FirstLine.GetPointWithdistansToEnd(SecondLine.GetLengthBM());
	}
	else return FirstLine.GetMiddlePoint();
}

Point Curve::CurveEndPoint(LineSegment FirstLine, LineSegment SecondLine) {
	if (!this->firstStraight) {
		return SecondLine.GetPointWithdistansToBeginning(FirstLine.GetLengthBM());
	}
	else return SecondLine.GetMiddlePoint();
}

Point Curve::GetCenter() { return this->center; }

double Curve::GetRadius() { return this->radius; }

double Curve::GetAngle() { return this->angle; }

bool Curve::IsCurveRight() { return this->curveOnTheRightSide; }

bool Curve::IsStraightFirst() { return this->firstStraight; }

double Curve::LenghtOfStraight() { return this->lenghtOfStraight; }