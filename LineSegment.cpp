#include "LineSegment.h"

LineSegment::LineSegment(){
}

LineSegment::LineSegment(Point beginning, Point end) {

	this->beginning = beginning;
	this->end = end;

}

double LineSegment::GetA() { return this->beginning.y - this->end.y; }

double LineSegment::GetB() { return this->end.x - this->beginning.x; }

double LineSegment::GetC() { return this->beginning.x*this->end.y - this->end.x*this->beginning.y; }

Point LineSegment::GetBeginning() { return this->beginning; }

Point LineSegment::GetEnd() { return this->end; }

double LineSegment::GetLengthBM() {
	return sqrt(pow(this->GetMiddlePoint().x - this->beginning.x, 2) + pow(this->GetMiddlePoint().y - this->beginning.y, 2));
}

double LineSegment::GetLengthME() {
	return sqrt(pow(this->GetMiddlePoint().x - this->end.x, 2) + pow(this->GetMiddlePoint().y - this->end.y, 2));
}

Point LineSegment::GetMiddlePoint() { 

	Point middlePoint;
	middlePoint.x = (beginning.x + end.x) / 2;
	middlePoint.y = (beginning.y + end.y) / 2;
	return middlePoint; 
}


Point LineSegment::GetPointWithdistansToBeginning(double distansToBeginning) {

	Point tempVector;

	tempVector.x = this->GetMiddlePoint().x - this->beginning.x;
	tempVector.y = this->GetMiddlePoint().y - this->beginning.y;

	Point point;
	point.x = tempVector.x * distansToBeginning / GetLengthME() + beginning.x;
	point.y = tempVector.y * distansToBeginning / GetLengthME() + beginning.y;
	return point;
}

Point LineSegment::GetPointWithdistansToEnd(double distansToEnd) {

	Point tempVector;

	tempVector.x = this->GetMiddlePoint().x - this->end.x;
	tempVector.y = this->GetMiddlePoint().y - this->end.y;

	Point point;
	point.x = tempVector.x * distansToEnd / GetLengthME() + end.x;
	point.y = tempVector.y * distansToEnd / GetLengthME() + end.y;
	return point;
}

LineSegment LineSegment::Perpendicular(Point crossPoint) {
	
	double A = this->GetB();
	double B = -this->GetA();
	double C = -A * crossPoint.x - B * crossPoint.y;
	Point point = crossPoint;
	if (B == 0) {
		point.y += 5;
	}
	else
	{
		point.x += 5;
		point.y = (-A*point.x - C) / B;
	}
	return LineSegment(crossPoint, point);
}

bool LineSegment::IsPointOnTheLine(Point point) {

	if (this->GetA() * point.x + this->GetB() * point.y + this->GetC() == 0) return true;
	else return false;
}

bool LineSegment::Collinear(LineSegment line) {
	if (line.GetA()*this->beginning.x + line.GetB()*this->beginning.y + line.GetC() == 0 &&
		line.GetA()*this->end.x + line.GetB()*this->end.y + line.GetC() == 0) 
		return true;
	return false;
}
