#ifndef LINESEGMENT_H
#define LINESEGMENT_H

#include "Point.h"
#include <math.h>

class LineSegment
{
public:
	LineSegment();
	LineSegment(Point a, Point b);
	double GetA();
	double GetB();
	double GetC();
	Point GetEnd();
	Point GetBeginning();
	double GetLengthME();
	double GetLengthBM();
	Point GetMiddlePoint();
	Point GetPointWithdistansToBeginning(double distansToBeginning);
	Point GetPointWithdistansToEnd(double distansToEnd);
	LineSegment Perpendicular(Point crossPoint);
	bool IsPointOnTheLine(Point point);
	bool Collinear(LineSegment line);
private:
	Point beginning;
	Point end;
	
};

#endif