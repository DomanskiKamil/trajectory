#ifndef CURVE_H
#define CURVE_H


#include "Point.h"
#include "LineSegment.h"

class Curve
{
public:
	Curve();
	Curve(LineSegment FirstLine, LineSegment SecondLine);
	Point GetCenter();
	double GetRadius();
	double GetAngle();
	bool IsCurveRight();
	bool IsStraightFirst();
	double LenghtOfStraight();
private:
	Point center;
	double radius;
	double angle;
	bool curveOnTheRightSide;
	double lenghtOfStraight;
	bool firstStraight;
	Point CurveStartPoint(LineSegment FirstLine, LineSegment SecondLine);
	Point CurveEndPoint(LineSegment FirstLine, LineSegment SecondLine);
	void SetLengthOfStraight(LineSegment FirstLine, LineSegment SecondLine);
};
#endif