#include <iostream>
#include <vector>
#include "Point.h"
#include "LineSegment.h"
#include "Curve.h"
#include "RobotPath.h"
using namespace std;

int main() {

	vector<Point>  pathPoints;
	pathPoints.push_back({ 0,0 });
	pathPoints.push_back({ 0,4 });
	pathPoints.push_back({ 0,5 });
	pathPoints.push_back({ 1,5 });
	pathPoints.push_back({ 2,3 });
	pathPoints.push_back({ 5,6 });
	pathPoints.push_back({ 6,2 });

	RobotPath robotPath;

	for (int i = 0; i < pathPoints.size() - 2; i++) {
		LineSegment line1 = LineSegment(pathPoints[i], pathPoints[i + 1]);
		LineSegment line2 = LineSegment(pathPoints[i + 1], pathPoints[i + 2]);

		if (i == 0) robotPath.AddStraight(line1.GetLengthBM());
		if (line1.Collinear(line2)) {
			robotPath.AddStraight(line1.GetLengthME() + line2.GetLengthBM());
			continue;
		}

		Curve curve;
		curve = Curve(line1, line2);
		robotPath.AddCurve(curve);

		if (i == pathPoints.size() - 3) robotPath.AddStraight(line2.GetLengthME());
	}


	robotPath.ShowPath();
	return 0;
}
