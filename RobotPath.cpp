#include "RobotPath.h"



RobotPath::RobotPath()
{
}


RobotPath::~RobotPath()
{
}

void RobotPath::AddStraight(double distance) {
	std::vector<double> Straight;
	Straight.push_back(distance);
	this->Path.push_back(Straight);
}

void RobotPath::AddCurve(Curve curve) {
	std::vector<double> CurveData;
	CurveData.push_back(curve.GetRadius());
	CurveData.push_back(curve.GetAngle());
	if(curve.IsCurveRight()) CurveData.push_back(1);
	else CurveData.push_back(0);
	if (curve.LenghtOfStraight() == 0) {
		this->Path.push_back(CurveData);
		return;
	}
	if (curve.IsStraightFirst()) {
		this->AddStraight(curve.LenghtOfStraight());
		this->Path.push_back(CurveData);
	}
	else {
		this->Path.push_back(CurveData);
		this->AddStraight(curve.LenghtOfStraight());
	}
}

void RobotPath::ShowPath() {
	for each (std::vector<double> Move in this->Path)
	{
		for each (double distance in Move)
		{
			std::cout << distance << " ";
		}
		std::cout << "\n";
	}
}